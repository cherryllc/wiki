# Don't Be Impatient

Being impatient in getting help on Thunix will score you no points at all in credibility...  For a few reasons:

* Thunix is ran by an all-volunteer team.  We don't get paid to do this.  We get some donations, but usually, not even enough to break even.
* Thunix is self-documented, generally (If it's not, it's a bug, and an issue should be submitted [here](https://tildegit.org/thunix/documentation) or a new wiki page made [here](https://tildegit.org/thunix/wiki).
* Thunix is maintained by a set of automation scripts.  If something isn't working, a PR [here](https://tildegit.org/thunix/ansible) can be done, to fix the problem.

So, if you ask a question in [#thunix](https://web.tilde.chat/?join=thunix), be patient.  There's generally not a lot of chatter, and it just takes a bit to catch up.  If it's been "too long", feel free to open an issue in [the pertinent repo](https://tildegit.org/thunix) or email the admin team at root at thunix.net.
