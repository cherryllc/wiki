man(8) tildegit.org" man(8)

NAME tildegit.org - a public web based source repository for the
tildeverse

SYNOPSIS https://tildegit.org

DESCRIPTION Tildegit is a web based source control system available for
the tildeverse, that runs gitea. Thunix uses tildegit.org for all source
control.

       The following git repos are used:

       * ansible (https://tildegit.org/thunix/ansible) This repo stores the configuration spec for all machines comprising Thunix.  The repo consists of declarative YAML
       files that describes the state the machines should be in.

       * www (https://tildegit.org/thunix/www) This repo stores the source code that makes up the https:/thunix.net website.  It's mostly php.

       * documentation (https://tildegit.org/thunix/documentation) This repo stores the man pages, and accompanying documentation for the system.

       * thunix_gopher (https://tildegit.org/thunix/thunix_gopher) This repo houses our gopher hole

SEE ALSO git(8), ansible(8)

BUGS No known bugs.

AUTHOR Uber Geek (ubergeek@thunix.net)

1.2 24 May 2019 man(8)
