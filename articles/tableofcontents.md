# Table of Contents

  - Chapter 1 - Thunix and You.
    1. What the fuck is Thunix?
      - History of Thunix
    2. Staff Bios
    3. Thunix Rules and Policies
    4. How can I help?
    5. Where is Thunix?
    6. Contributions Directory

  - Chapter 2 - SSH Shit.
    1. To Be Linked. (WIP)

  - Chapter 3 - Contributing
    1. What is "Contributing"?
      - Git Crash Course
      - History and Usage
    2. How to Contrib.
      - General "Rule of Thumb"s
      - Workflow
      - Etiquette
      - Methods of.
    3. Contrib'ing to Thunix
      - Guidelines & Proceedures
      - Methods Available
      - Tildegit & Ansible
      - Formatting

  - Chapter 4 - The Wiki
    1. TBA/Planned. See Contributions Directory.
